package com.lcmw.cqe.memory;

import java.util.List;
import java.util.ArrayList;

public class ManualDeleter {
	private static List<Delete> deletes = new ArrayList<>();
	
	public static void add(Delete del) {
		deletes.add(del);
	}
	
	public static void deleteAll() {
		for(Delete del:deletes) del.delete();
		
		deletes.clear();
	}

}
