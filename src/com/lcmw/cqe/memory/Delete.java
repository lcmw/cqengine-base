package com.lcmw.cqe.memory;

public abstract class Delete {
	public Delete() {
		ManualDeleter.add(this);
	}
	
	public abstract void delete();
}