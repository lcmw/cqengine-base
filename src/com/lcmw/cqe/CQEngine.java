package com.lcmw.cqe;

import org.lwjgl.glfw.GLFW;

import com.lcmw.cqe.memory.ManualDeleter;
import com.lcmw.cqe.util.Display;

/**
 * A wonderful assortment of stuff
 * */
public class CQEngine {
	
	public static void exit() {
		GLFW.glfwSetWindowShouldClose(GLFW.glfwGetCurrentContext(), true);
	}
	
	public static void close() {
		Display.destroy();
		ManualDeleter.deleteAll();
	}
}
