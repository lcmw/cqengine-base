package com.lcmw.cqe.renderer.data;

public class GLModel {
	private int vaoID, vCount;
	
	public GLModel(int vaoID, int vCount) {
		this.vaoID = vaoID;
		this.vCount = vCount;
	}

	public int getVaoID() {
		return vaoID;
	}

	public void setVaoID(int vaoID) {
		this.vaoID = vaoID;
	}

	public int getvCount() {
		return vCount;
	}

	public void setvCount(int vCount) {
		this.vCount = vCount;
	}
}
